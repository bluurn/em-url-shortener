Scramblr = Struct.new(:wc) do
  def self.call
    new(10).call
  end

  def call
    wc.times.map { alphabet.sample }.join
  end

  private

  def alphabet
    (0..9).to_a + ('a'..'z').to_a + ('A'..'Z').to_a
  end
end
