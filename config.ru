require 'rubygems'
require 'bundler/setup'
require 'rack/fiber_pool'
require 'rack/contrib'

require './app'

use Rack::FiberPool
use Rack::CommonLogger
use Rack::PostBodyContentTypeParser
run App
