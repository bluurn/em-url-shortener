require 'sinatra/base'
require 'redis/connection/synchrony'
require 'redis'
require './lib/scramblr'

class App < Sinatra::Base  
  def initialize
    super
    @redis = EventMachine::Synchrony::ConnectionPool.new(size: 2) do
      Redis.new
    end
    @namespace = 'url-shortener'
  end

  post '/' do
    loop do
      short_url = Scramblr.()
      unless @redis.get([@namespace, short_url].join(':'))
        @redis.set([@namespace, short_url].join(':'), params['long_url'])
        return { url: url("/#{short_url}") }.to_json
      end
    end
  end

  get '/*' do |short_url|
    url = @redis.get([@namespace, short_url].join(':'))
    if url
      redirect to(url)
    else
      halt(404)
    end
  end
end
